package main;

public class RockPaperScissors {

	private Player p1;
	private Player p2;

	private int runs;
	private int p1Wins;
	private int p2Wins;
	private int ties;

	public RockPaperScissors(Player p1, Player p2) {
		if (p1 == null || p2 == null) {
			throw new IllegalArgumentException("Invalid argument");
		}

		this.p1 = p1;
		this.p2 = p2;
	}

	/**
	 * Spiel starten
	 */
	public void startGame(int runs) {
		this.runs = runs;
		this.p1Wins = 0;
		this.p2Wins = 0;
		this.ties = 0;

		for (int i = 0; i < runs; i++) {
			// Aktion ausf�hren
			Move p1Move = p1.makeMove();
			Move p2Move = p2.makeMove();

			// beide Aktionen vergleichen
			int compareMoves = p1Move.compareMoves(p2Move);
			switch (compareMoves) {
			case 0: // Verloren
				p2Wins++;
				break;
			case 1: // Unentschieden
				ties++;
				break;
			case 2: // Gewonnen
				p1Wins++;
				break;
			}
		}
	}

	/**
	 * Ausgabe der Spielergebnisse
	 */
	public void printoutResults() {
		System.out.println("Siege von Spieler 1 " + p1Wins);
		System.out.println("Siege von Spieler 2 " + p2Wins);
		System.out.println("Unentschieden " + ties);

		if (getCompletedRuns() < runs) {
			System.out.println("Unentschieden " + ties);
		}
	}

	public int getCompletedRuns() {
		return p1Wins + p2Wins + ties;
	}
}
