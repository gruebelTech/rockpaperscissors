package main;

public enum Move {
	ROCK, PAPER, SCISSORS;

	/**
	 * Vergleicht die beiden Aktionen
	 * 
	 * @return 0 Verloren, 1 Unentschieden, 2 Gewonnen, -1 Fehlerfall
	 */
	public int compareMoves(Move move) {

		// Fehlerfall
		if (move == null) {
			return -1;
		}

		// Unentschieden
		if (this == move) {
			return 1;
		}

		// Gewinnnen oder verlieren
		switch (this) {
		case ROCK:
			if (move == SCISSORS) {
				return 2;
			} else {
				return 0;
			}
		case PAPER:
			if (move == ROCK) {
				return 2;
			} else {
				return 0;
			}
		case SCISSORS:
			if (move == PAPER) {
				return 2;
			} else {
				return 0;
			}
		}

		// Fehlerfall
		return -1;
	}
}
