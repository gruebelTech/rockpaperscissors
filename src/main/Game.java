package main;

public class Game {
	public static void main(String[] args) {
		int runs = 100;
		Player p1 = new Player();
		Player p2 = new Player();
		p2.setHandicap(Move.ROCK);

		RockPaperScissors game = new RockPaperScissors(p1, p2);
		game.startGame(runs);
		game.printoutResults();
	}
}
