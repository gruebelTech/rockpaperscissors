package main;
import java.security.SecureRandom;

public class Player {

	private Move restrictedMove;

	/**
	 * Spieler auf spezifische Aktion beschränken
	 */
	public void setHandicap(Move move) {
		this.restrictedMove = move;
	}

	/**
	 * Ausführen einer Aktion
	 */
	public Move makeMove() {

		// Falls Spieler auf einzelne Aktion beschränkt wurde
		if (restrictedMove != null) {
			return restrictedMove;
		}

		Move[] moves = Move.values();
		SecureRandom random = new SecureRandom();
		int index = random.nextInt(moves.length);

		return moves[index];
	}

}
