package test;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import main.Move;
import main.Player;

import org.junit.Test;

public class PlayerTest {

	private List<Move> moveList = Arrays.asList(Move.values());

	@Test
	public void playerMoveShouldBeValid() {
		Player player = new Player();

		for (int i = 0; i < 100; i++) {
			assertTrue("Player makes a valid move", moveList.contains(player.makeMove()));
		}
	}

	@Test
	public void playerRestrictionShouldBeValid() {
		Player player = new Player();
		player.setHandicap(Move.PAPER);

		assertTrue("Player can only use the move Paper", player.makeMove() == Move.PAPER);

		player.setHandicap(Move.ROCK);

		assertTrue("Player can only use the move Paper", player.makeMove() == Move.ROCK);

		player.setHandicap(Move.SCISSORS);

		assertTrue("Player can only use the move Paper", player.makeMove() == Move.SCISSORS);

		player.setHandicap(null);

		for (int i = 0; i < 100; i++) {
			assertTrue("Player makes random move again", moveList.contains(player.makeMove()));
		}
	}

}
