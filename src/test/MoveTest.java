package test;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import main.Move;

import org.junit.Test;

public class MoveTest {

	private List<Move> moveList = Arrays.asList(Move.values());

	@Test
	public void compareMoveShouldReturnCorrect() {

		// Papier
		assertTrue("Paper compare with null returns -1", Move.PAPER.compareMoves(null) == -1);
		assertTrue("Paper compare with Paper returns 1", Move.PAPER.compareMoves(Move.PAPER) == 1);
		assertTrue("Paper compare with Rock returns 2", Move.PAPER.compareMoves(Move.ROCK) == 2);
		assertTrue("Paper compare with Scissors returns 0",
				Move.PAPER.compareMoves(Move.SCISSORS) == 0);

		// Stein
		assertTrue("Rock compare with null returns -1", Move.ROCK.compareMoves(null) == -1);
		assertTrue("Rock compare with Paper returns 0", Move.ROCK.compareMoves(Move.PAPER) == 0);
		assertTrue("Rock compare with Rock returns 1", Move.ROCK.compareMoves(Move.ROCK) == 1);
		assertTrue("Rock compare with Scissors returns 2",
				Move.ROCK.compareMoves(Move.SCISSORS) == 2);

		// Schere
		assertTrue("Scissors compare with null returns -1", Move.SCISSORS.compareMoves(null) == -1);
		assertTrue("Scissors compare with Paper returns 2",
				Move.SCISSORS.compareMoves(Move.PAPER) == 2);
		assertTrue("Scissors compare with Rock returns 0",
				Move.SCISSORS.compareMoves(Move.ROCK) == 0);
		assertTrue("Scissors compare with Scissors returns 1",
				Move.SCISSORS.compareMoves(Move.SCISSORS) == 1);
	}

}
