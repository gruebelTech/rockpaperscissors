package test;

import static org.junit.Assert.assertTrue;
import main.Player;
import main.RockPaperScissors;

import org.junit.Test;

public class RockPaperScissorsTest {

	@Test(expected = IllegalArgumentException.class)
	public void useOfNullShouldThrowNPE() {
		RockPaperScissors game = new RockPaperScissors(null, null);
	}

	@Test
	public void allRunsShouldBeSuccessful() {
		int runs = 0;
		Player p1 = new Player();
		Player p2 = new Player();

		RockPaperScissors game = new RockPaperScissors(p1, p2);
		game.startGame(runs);

		assertTrue("The game ends with expected Runs", game.getCompletedRuns() == runs);

		runs = 10000;
		game.startGame(runs);

		assertTrue("The game ends with expected Runs", game.getCompletedRuns() == runs);
	}
}
